import React from 'react';
import { Navbar } from './components/Navbar';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { TodosPage } from './pages/TodosPage';
import { AboutPage } from './pages/AboutPage';
import { UsersPage } from './pages/UsersPage';
import { MemoryPage } from './pages/MemoryPage';

import { ThemeContext, themes } from './contexts/theme';


const App: React.FC = () => {
  return (
    <ThemeContext.Provider value={themes.dark}>
      <BrowserRouter>
        <Navbar />
        <div className="container">
          <Switch>
            <Route component={TodosPage} path="/" exact />
            <Route component={AboutPage} path="/about" />
            <Route component={UsersPage} path="/users" />
            <Route component={MemoryPage} path="/memory-game" />
          </Switch>
        </div>
      </BrowserRouter>
    </ThemeContext.Provider>
  );
}

export default App;
