import React, { useState, useEffect, useDebugValue } from 'react';
import { from, of, Observable } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';
import { switchMap, catchError, mergeAll, scan, last, map, count, withLatestFrom, delay, share } from 'rxjs/operators';
import { IUser } from '../interfaces';
import { UserList } from '../components/UserList';
import { ProgressBar } from '../components/ProgressBar';

const USER_IDS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];


const getUserByIds = (ids: Array<number>): {users$: Observable<any>, ratio$: Observable<any>} => {
  const array$ = from(ids.map((id) => 
    fromFetch(`https://swapi.co/api/people/${id}/`)
      .pipe(
        delay(300 * id),
        switchMap(response => {
          if (response.ok) {
            // OK return data
            return  response.json();
          } else {
            // Server is returning a status requiring the client to try something else.
            return of({ error: true, message: `Error ${response.status}` });
          }
        }),
        catchError(err => {
          // Network or other error, handle appropriately
          console.error(err);
          return of({ error: true, message: err.message })
        }),
        map((user) => ({
          id,
          ...user
        }))
       )
  ));

  const requests$ = array$.pipe(mergeAll(), share());
  const count$ = array$.pipe(count());

  const users$ = requests$.pipe(
    scan((acc, v) => acc.concat(v as IUser[]), [] as IUser[]),
    last(),
  );

  const ratio$ = requests$.pipe(
    scan((current) => current + 1, 0),
    withLatestFrom(count$, (current, count) => (current / count * 100))
  );
  
  return {
    users$,
    ratio$
  };
}

const useFetchUsers = () => {
  const [users, setUsers] = useState<IUser[]>([]);
  const [progress, setProgress] = useState<number>(0);

  useEffect(() => {
    const {users$, ratio$} = getUserByIds(USER_IDS);
    const userObserver = users$.subscribe(setUsers);
    const progressObserver = ratio$.subscribe(setProgress);

    return () => {
      userObserver.unsubscribe();
      progressObserver.unsubscribe();
    }
  }, []);

  useDebugValue([users, progress]);

  return {users, progress};
}

export const UsersPage: React.FC = () => {
  const {users, progress} = useFetchUsers();

  return <>
    <h1>Users Page</h1>
    <ProgressBar value={progress}/>
    <UserList users={users} />
  </>
}
