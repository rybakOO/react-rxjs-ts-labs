import React, { useEffect, useState } from 'react';
import { MemorySettings } from '../components/MemorySettings';
import { MemoryGrid } from '../components/MemoryGrid';
import runMemory from '../observables/memory';

const SIZES = [3, 4, 5];
const SIZE_DEFAULT = SIZES[0];

const generateGrid = (size: number) => {
  return Array(size).fill(null).map(
    (v, index) => Array(size).fill(null).map((v, subindex) => (subindex + 1) + size * index)
  );
};

interface ISetting {
  stop: () => void,
  nextSelect: (id: number) => void
}

export const MemoryPage: React.FC = () => {
  const [grid, setGrid] = useState<number[][]>(generateGrid(SIZE_DEFAULT));
  const [info, setInfo] = useState<string>('Train Your Memory!');
  const [itemId, setItemId] = useState<number>();
  const [setting, setSetting] = useState<ISetting>();
  const [runGame, setStatusGame] = useState<boolean>();

  const handleSelectItem = (e: React.MouseEvent) => {
    const target = e.target as HTMLElement;
    if(setting && target) {
      setting.nextSelect(Number(target.getAttribute('id')));
    }
  }

  const handleSetSetting = (size:number) => {
    if(setting) setting.stop();
    setGrid(generateGrid(size));
  };

  const handleStartGame = () => {
    const _setting = runMemory(grid.length, setInfo, setItemId, () => {
      _setting.stop();
      setStatusGame(false);
      setInfo('Game Over');
    });
    setStatusGame(true);
    setSetting(_setting);
  }

  return <>
    <h2>Memory game</h2>

    {!runGame ? 
      <MemorySettings setSettings={handleSetSetting} startGame={handleStartGame} sizes={SIZES} /> : null}
    <h6>{info}</h6>
    <MemoryGrid grid={grid} itemIdActive={itemId} handlerSelect={handleSelectItem} />
  </>;
}