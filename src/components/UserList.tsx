import React from 'react';
import { IUser } from '../interfaces';

type UsersProps ={
  users: IUser[],
}

export const UserList: React.FC<UsersProps> = ({ users }) => {
  if(!users.length) return <p className="center">Пока пользователь нет!</p>

  return (
    <ul className="collection">
      {users.map(user => {
        return (
          <li key={user.id} className="collection-item">
            <h5 className="title">{user.name}</h5>
            <p>
              <b>Gender:</b> {user.gender} <br/>
              <b>Height:</b> {user.height} <br/>
              <b>Mass:</b> {user.mass} <br/>
              <b>Hair color:</b> {user.hair_color} <br/>
              <b>Skin color:</b> {user.skin_color}
            </p>
          </li>
        );
      })}
    </ul>
  )
}