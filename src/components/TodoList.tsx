import React from 'react';
import { ITodo } from '../interfaces';

type TodoListProps ={
  todos: ITodo[],
  onToggle(id: number): void
  onRemove(id: number): void
}

export const TodoList: React.FC<TodoListProps> = ({ todos, onRemove, onToggle }) => {
  if(!todos.length) return <p className="center">Пока дел нет!</p>

  const removeHandler = (id: number, event: React.MouseEvent) => {
    event.preventDefault();
    onRemove(id); 
  }

  return (
    <ul>
      {todos.map(todo => {
        return (
          <li key={todo.id} className={`todo ${todo.completed ? 'completed' : ''} mt`}>
            <label>
              <input type="checkbox" checked={todo.completed} onChange={onToggle.bind(null, todo.id)}/>
              <span>{todo.title}</span>
              <i className="material-icons red-text" onClick={removeHandler.bind(null, todo.id)}>delete</i>
            </label>
          </li>
        );
      })}
    </ul>
  )
}