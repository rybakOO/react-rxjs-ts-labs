import React from 'react';

interface IMemoryGridProps {
    grid: number[][],
    itemIdActive: number | undefined,
    handlerSelect: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
}

export const MemoryGrid:React.FC<IMemoryGridProps> = ({ 
  grid,
  itemIdActive, 
  handlerSelect 
}) => (
    <div id="grid" className="grid parent perspective">
      {grid.map((row, index) => (
        <div key={index}>
          {row.map((value) =>
            <button 
              key={value} 
              onClick={handlerSelect}
              className={`child ${itemIdActive === value ? 'btn' : 'btn-flat' }`} 
              id={String(value)}></button>
          )}
        </div>
      ))}
    </div>
);
