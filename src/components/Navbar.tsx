import React, { useContext } from 'react';
import {NavLink, Link} from 'react-router-dom';

import { ThemeContext } from '../contexts/theme';

export const Navbar: React.FunctionComponent = () => {
  const theme = useContext(ThemeContext)

  return (
    <nav style={{ background: theme.background, color: theme.foreground }}>
      <div className="nav-wrapper teal darken-4 pd">
        <Link to="/" className="brand-logo">Logo</Link>
        <ul className="right hide-on-med-and-down">
          <li><NavLink to="/">Список дел</NavLink></li>
          <li><NavLink to="/about">Информация</NavLink></li>
          <li><NavLink to="/users">Пользователи</NavLink></li>
          <li><NavLink to="/memory-game">Игра памяти</NavLink></li>
        </ul>
      </div>
    </nav>
  )
}