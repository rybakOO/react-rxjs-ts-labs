import React from 'react';

interface IMemorySettingsProps  {
  setSettings: (size: number) => void,
  startGame: () => void,
  sizes: number[]
}

const ICONS:{
  [propName: number]: string;
} = {
  3: 'looks_one',
  4: 'looks_two',
  5: 'looks_3'
};

export const MemorySettings: React.FC<IMemorySettingsProps> = ({ setSettings, sizes, startGame }) => 
  <>
    {sizes.map((size) => (
      <button key={size} type="button" className="waves-effect waves-light btn-small" onClick={() => setSettings(size)}>
        <i className="material-icons left">
          {ICONS[size]}
        </i> 
        {size}x{size}
      </button>
    ))}
    <button type="button" className="waves-effect waves-light btn-small" onClick={startGame}>
      Start
    </button>
  </>