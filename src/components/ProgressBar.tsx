import React from 'react';

interface IProgressBarProps  {
  value: number
}

export const ProgressBar:React.FC<IProgressBarProps> = ({ value }) => (
  <div className="progress">
      <div className="determinate" style={{
        width: `${value}%`
      }}></div>
    </div>
)