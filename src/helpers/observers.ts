export const observerA = {
  next: (v: any) => console.log('[A] Next:', v),
  error: (err: any) => console.log('[A] Error:', err),
  complete: () => console.log('[A] Complete:'),
}

export const observerB = {
  next: (v: any) => console.log('[B] Next:', v),
  error: (err: any) => console.log('[B] Error:', err),
  complete: () => console.log('[B] Complete:'),
}

export const observerC = {
  next: (v: any) => console.log('[C] Next:', v),
  error: (err: any) => console.log('[C] Error:', err),
  complete: () => console.log('[C] Complete:'),
}

export const observerD = {
  next: (v: any) => console.log('[D] Next:', v),
  error: (err: any) => console.log('[D] Error:', err),
  complete: () => console.log('[D] Complete:'),
}

export function log(...args: any[]) {
  const colorized = args;
  console.log(...colorized);
}
