// function mergeObjects<T, R>(a: T, b: R): T & R {
//   return Object.assign({}, a, b);
// }

function mergeObjects<T extends object, R>(a: T, b: R): T & R {
  return Object.assign({}, a, b);
}

const merged = mergeObjects({name: "Oleg"}, {age: 800});

console.log(merged.name);
console.log(merged.age);


//  ===============================================================

interface ILength {
  length: number
}

function withCount<T extends ILength>(value: T): {value: T, count: string} {
  return {
    value,
    count: `В этом обьекте ${value.length} симовлов`
  }
}

console.log('withCount', withCount([]), withCount('length'));

//  ===============================================================

function getObjectValue<T extends object, R extends keyof T>(obj: T, key: R) {
  return obj[key];
}

console.log('getObjectValue', getObjectValue({length: 100}, 'length'));
// console.log('getObjectValue', getObjectValue({length: 100}, 'name'));

//  ===============================================================

class Collection<T extends number | string | boolean> {
  constructor(private _items: T[] = []) {}

  add(item: T) {
    this._items.push(item);
  }

  remove(item: T) {
    this._items = this._items.filter((_item) => _item !== item);
  }

  get items(): T[] {
    return this._items
  }
}

const strings = new Collection<string>(['I', 'like', 'to']);
strings.add('test');
strings.remove('like');
console.log(strings.items);

// const objects = new Collection([{a:1}, {b:1}]);
// objects.remove({b:1});
// console.log(objects.items);


//  ===============================================================

interface Car {
  model: string,
  year: number
}

function createAndValidateCar(model: string, year: number): Car {
  const car: Partial<Car> = {};

  if (model.length > 3) {
    car.model = model;
  }

  if (year > 2000) {
    car.model = model;
  }

  return car as Car;
}

//  ===============================================================

const cars: Readonly<Array<string>> = ['Fort', 'Audi'];

const ford: Readonly<Car> = {
  model: 'Ford',
  year: 2020
}




export default {}