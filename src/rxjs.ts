import {interval, fromEvent, pipe, range, Subject, BehaviorSubject, ReplaySubject, PartialObserver, Observable, Observer, AsyncSubject, ConnectableObservable, Subscription, merge, timer, from, of, zip, empty } from 'rxjs';
import {filter, map, take, scan, tap, takeLast, reduce, multicast, refCount, share, delay, first, takeUntil, shareReplay, switchMap, mapTo, mergeMap, toArray } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { observerA, observerB, log } from './helpers/observers';

const rxjsBtn:HTMLButtonElement = document.getElementById('rxjs') as HTMLButtonElement;
const display = document.querySelector('#problem .result');


 

// const people = [
//   {name: 'Vladilen', age: 25},
//   {name: 'Elena', age: 17},
//   {name: 'Ivan', age: 18},
//   {name: 'Igor', age: 14},
//   {name: 'Lisa', age: 32},
//   {name: 'Irina', age: 23},
//   {name: 'Oleg', age: 20}
// ];

// rxjsBtn?.addEventListener('click', () => {
//   rxjsBtn.disabled = true;
//   interval(1000)
//     .pipe(
//       take(people.length),
//       filter((v) => people[v].age >= 18),
//       map((v) => people[v].name),
//       scan((acc, v) => acc.concat(v), [] as Array<string>)
//     )
//     .subscribe(
//       (res) => {
//         if(display) {
//           display.textContent = res.join(', ');
//         }
//       },
//       null,
//       () => {
//         rxjsBtn.disabled = false;
//       }
//     )
// });


// ------------------------------------------------------------------------------------
// const canvas: HTMLCanvasElement = document.querySelector('canvas') as HTMLCanvasElement;

// fromEvent<MouseEvent>(canvas, 'mousemove')
//   .pipe(
//     map((e: MouseEvent) => {
//       const target: HTMLCanvasElement = e.target as HTMLCanvasElement;
//       return {
//         x: e.offsetX,
//         y: e.offsetY,
//         ctx: target.getContext('2d')
//       }
//     })
//   )
//   .subscribe({
//     next: (position) => {
//       position.ctx?.fillRect(position.x, position.y, 2, 2)
//     }
//   });


// const clear$ = fromEvent(document.getElementById('clear') as HTMLButtonElement, 'click')

// clear$.subscribe({
//   next() {
//     canvas.getContext('2d')?.clearRect(0, 0, canvas.width, canvas.height);
//   }
// })
// ------------------------------------------------------------------------------------

// const timerStream = interval(1000).subscribe();

// timerStream.unsubscribe();


// range(1, 2).subscribe((e) => console.log(e));

// ------------------------------------------------------------------------------------

// document.addEventListener('click', () => {
//   const stream$ = new Subject();

//   stream$.subscribe((m) => console.log('Subject', m));

//   stream$.next('Hello');

// });

// document.addEventListener('click', () => {
//   const stream$ = new BehaviorSubject('Test');

//   stream$.next('Hello');
//   stream$.next('Hello1');
//   stream$.next('Hello2');

//   stream$.subscribe((m) => console.log('BehaviorSubject', m));

// });

// document.addEventListener('click', () => {
//   const stream$ = new ReplaySubject();

//   stream$.next('Hello');
//   stream$.next('Hello1');
//   stream$.next('Hello2');

//   stream$.subscribe((m) => console.log('ReplaySubject', m));

// });


// ------------------------------------------------------------------------------------

// const stream$ = interval(1000)
//   .pipe(
//     tap(v => console.log('Tap', v)),
//     map((v) => v * 3),
//     filter((v) => v % 2 === 0),
//     take(5),
//     takeLast(3),
//     scan((acc, v) => acc + v, 0),
//     reduce((acc, v) => acc + v, 0)
//   );

// stream$.subscribe({
//   next: v => console.log('Next', v),
//   complete: () => console.log('Complete')
// });

// ------------------------------------------------------------------------------------

/**
 * Subject: an Observable and Observer hydrid
 * 
 * -^-a-b-c-d|
 * ----^b-c-d|
 */

// const observable = interval(1000).pipe(take(5));

// class BridgeObserver {
//   private observers: Array<PartialObserver<any>> = [];

//   next(v:any) {
//     this.observers.forEach((o) => o.next && o.next(v));
//   }

//   error(v:any) {
//     this.observers.forEach(o => o.error && o.error(v));
//   }

//   complete() {
//     this.observers.forEach(o => o.complete && o.complete());
//   }

//   subscribe(observer: PartialObserver<any>) {
//     this.observers.push(observer);
//   }
// }

// // const bridge = new BridgeObserver();
// const bridge = new Subject();

// bridge.subscribe(observerA);

// observable.subscribe(bridge);

// setTimeout(() => {
//   bridge.subscribe(observerB);
// }, 3000);

/**
 * Using a Subejct as an Event Bus
 * 
 * ^-abc----d---->
 * ------^-d----->
 */

// const event = new Subject();

// event.subscribe(observerA);

// setTimeout(() => {
//   event.subscribe(observerB);
// }, 200);

// event.next(1);
// event.next(2);
// event.next(3);

// setTimeout(() => {
//   event.next(10);
// }, 1000);

/**
 * BehaviorSubject: representing a value over time
 */

// const subject = new BehaviorSubject(0);

// subject.subscribe(observerA);

// log('observerA subscribed');

// subject.next(1);
// subject.next(2);
// subject.next(3);
// subject.next(4);
// subject.next(5);

// // age vs Birthdays

// /*
// 0---1---2---3----------------------
//  0..1...2...3...
// */

// setTimeout(() => {
//   subject.subscribe(observerB);
//   log('observerB subscribed');
// }, 2000)

/**
 * ReplaySubject: representing a value over time
 */

// const subject = new ReplaySubject(3);

// subject.subscribe(observerA);

// log('observerA subscribed');

// subject.next(1);
// subject.next(2);
// subject.next(3);
// subject.next(4);
// subject.next(5);

// setTimeout(() => {
//   subject.subscribe(observerB);
//   log('observerB subscribed');
// }, 2000)


/**
 * AsyncSubject: represention a computation that yields a final
 */

// const subject = new AsyncSubject();

// subject.subscribe(observerA);
// log('observerA subscribed');

// setTimeout(() => subject.next(1), 100);
// setTimeout(() => subject.next(2), 200);
// setTimeout(() => subject.next(3), 300);
// setTimeout(() => subject.next(4), 400);
// setTimeout(() => subject.complete(), 450);


// setTimeout(() => {
//   subject.subscribe(observerB);
//   log('observerB subscribed');
// }, 500);


/**
 * Connection operator: multicast and connect
 */

// const connectableObservable = interval(1000)
//   .pipe(
//     take(6),
//     multicast(new ReplaySubject(100)),
//     // multicast(new ReplaySubject(1)),
//     // multicast(new ReplaySubject(0)),

//     // multicast(new BehaviorSubject(111)),
//     // multicast(new AsyncSubject()),
//   ) as ConnectableObservable<number>;

// connectableObservable.connect();
// connectableObservable.subscribe(observerA);


// setTimeout(() => {
//   connectableObservable.subscribe(observerB);
// }, 4500);

// --------------------------------------------------------

// const connectableObservable = interval(1000)
//   .pipe(
//     tap((x) => log('source ' + x)),
//     multicast(new ReplaySubject(3)),
//     refCount()
//   );

// let subA = connectableObservable.subscribe(observerA);
// let subB: Subscription;

// setTimeout(() => {
//   subB = connectableObservable.subscribe(observerB);
// }, 4500);

// setTimeout(() => {
//   subA.unsubscribe();
//   subB.unsubscribe();
// }, 6000);

// ------------------------------------------------------


// publish = multicast(new Subject())
// share = publish().refCount()


// const connectableObservable = interval(1000)
//   .pipe(
//     tap((x) => log('source ' + x)),
//     share()
//   );

// let subA = connectableObservable.subscribe(observerA);
// let subB: Subscription;

// setTimeout(() => {
//   subB = connectableObservable.subscribe(observerB);
// }, 4500);

// setTimeout(() => {
//   subA.unsubscribe();
//   subB.unsubscribe();
// }, 6000);


// ------------------------------------------------------

// function subjectFactory() {
//   return new Subject();
// }

// const shared = interval(1000)
//   .pipe(
//     take(6),
//     tap(x => log(`source ${x}`)),
//     multicast(subjectFactory),
//     refCount()
//   )

// let subA = shared.subscribe(observerA);
// log('subscribed A');

// let subB: Subscription;
// setTimeout(() => {
//   subB = shared.subscribe(observerB);
//   log('subscribed B');
// }, 2000);

// setTimeout(() => {
//   subA.unsubscribe();
//   log('unsubscribed A');
// }, 5000);

// setTimeout(() => {
//   subB.unsubscribe();
//   log('unsubscribed B');
// }, 7000);

// setTimeout(() => {
//   subA = shared.subscribe(observerA);
//   log('subscribed A');
// }, 8000);


// ------------------------------------------------------


// function subjectFactory() {
//   return new Subject();
// }

// const result = interval(1000)
//   .pipe(
//     take(6),
//     tap(x => log(`source ${x}`)),
//     map(x => Math.random()),
//     multicast(subjectFactory, function selector(shared) {
//       const sharedDelayed = shared.pipe(
//         delay(500),
//         map(t => `2: ${t}`)
//         );
//         const merged = merge(shared, sharedDelayed);
//         return merged;
//       }),
//   );

// const sub = result.subscribe(observerA);

// const click$ = Observable.create(
//   function subscribe(observer: Observer<any>) {
//     const listener = function(ev: any) {
//       observer.next(ev);
//     };
//     document.addEventListener('click', listener);

//     return function unsubscribe() {
//       document.removeEventListener('click', listener)
//     };
//   }
// );

// click$.subscribe(observerA);

// ------------------------------------------------------------

/* const click$ = fromEvent<MouseEvent>(document, 'click');

const subscription = click$.subscribe((ev: MouseEvent) => {
  console.log(ev.clientX);
});

setTimeout(function () {
  subscription.unsubscribe();
}, 4000);
 */
// ------------------------------------------------------------

// const click$ = fromEvent(document, 'click');

// const fiveClicks$ = click$.pipe(take(5));

// /*
// click$          --c------c---c-c----c---c-
// sixClicks$ .    --c------c---c-c----c|
// */


// fiveClicks$.subscribe(observerA);


// ------------------------------------------------------------
// const click$ = fromEvent(document, 'click');

// const four$ = interval(4000).pipe(first());

// /*
// click$          --c------c---c-c-----c---c---c-
// four$           -----------------0|
// clickUntilFour$ --c------c---c-c--|
// */

// const clickUntilFour$ = click$.pipe(takeUntil(four$));

// clickUntilFour$.subscribe(observerA);
// ------------------------------------------------------------

// const isOnline$ = timer(0, 2000).pipe(
//   map((i: number) => i%2 === 0),
//   shareReplay(1),
//   // share(),
// );

// isOnline$.subscribe(observerA);

// const click$ = fromEvent(document, 'click');

// click$
//   .pipe(
//     // mergeMap(() =>
//     switchMap(() =>
//       isOnline$.pipe(
//         first(),
//         filter((is: boolean) => is),
//         // first(),
//       )
//     ),
//     mapTo('click'),
//   )
//   .subscribe(observerB);

//----------------------------------------------


// const subject$ = new Subject<number>();

// document.addEventListener('click', () => subject$.next(1));

// fetch('https://jsonplaceholder.typicode.com/users/0')
//   .then(res => res.json())
//   .then(() => subject$.next(1));

// const count$ = subject$.pipe(
//   scan((acc, x:number) => acc + x, 0),
// );

// count$.subscribe(observerA);


//----------------------------------------------
// const click$ = fromEvent(document, 'click');

// const res$ = from(
//   fetch('https://my-json-server.typicode.com/typicode/demo/posts')
//     .then(res => res.json())
// );

// const count$ = merge(click$, res$).pipe(
//                  map((arr) => {
//                   console.log(arr);
//                   return 1
//                  }),
//                  scan((acc, x) => acc + x, 0),
// );

// count$.subscribe(observerA);

//---------------------------------------------- 

// const click$ = fromEvent<MouseEvent>(document, 'click');

// const x$ = click$.pipe(
//   map((ev: MouseEvent) => {
//     console.log(ev);
//     return ev.clientX
//   }),
// );

// x$.subscribe(observerA);

//---------------------------------------------- 

// const userData$ = ajax({
//   url: 'https://my-json-server.typicode.com/typicode/demo/posts',
//   method: 'GET',
// });

// const click$ = fromEvent(document, 'click');

// const resWhenClick$ = click$.pipe(
//   mergeMap(_ => userData$),
//   map(data => data['response']),
//   );
  
//   resWhenClick$.subscribe(observerA);

//---------------------------------------------- 

// const length$ = of(5, 5);
// const width$  = of(10, 5);
// const height$ = of(2, 2.5);

// const volume$ = zip(length$, width$, height$,
// // const volume$ = combineLatest(length$, width$, height$,
//   (length: number, width: number, height: number) => length * width * height,
// );

// volume$.subscribe(observerA);
//---------------------------------------------- 

// document.body.innerHTML = '<div style="width: 10px; height: 10px; background-color: red;"></div>';
// const dotElem = document.getElementsByTagName('div')[0];

// function updateDot(x:number, y:number) {
//   dotElem.style.marginLeft = `${x}px`;
//   dotElem.style.marginTop = `${y}px`;
// }

// const click$ = fromEvent<MouseEvent>(document, 'click');

// click$.subscribe((e: MouseEvent) => updateDot(e.clientX, e.clientY));

// const res$ = click$.pipe(
//   switchMap(ev => ajax({
//     url: 'https://jsonplaceholder.typicode.com/users/1',
//     method: 'GET',
//   })),
//   map((data) => data['response']),
// );

// res$.subscribe(observerA);


// ---------------------------------------------- 

// const resume$ = new Subject<boolean>();

// const res$ = resume$.pipe(
//   switchMap(resume =>
//     resume ?
//       interval(2000) :
//       empty(),
//   ),
//   tap(x => log('request it! ' + x)),
//   switchMap(ev => ajax({
//     url:    'https://jsonplaceholder.typicode.com/users/2',
//     method: 'GET',
//   })),
//   map(data => data[ 'response' ].name),
// );

// res$.subscribe(observerA);

// resume$.next(false);
// setTimeout(() => resume$.next(true), 500);
// setTimeout(() => resume$.next(false), 7000);

// ---------------------------------------------- 
// const clock$ = interval(500).pipe(
//   share(),
//   take(7)
// );

// const randomNum$ = clock$.pipe(
//   map(i => Math.random() * 100),
//   share(),
// );

// const smallNum$ = randomNum$.pipe(
//   filter(x => x <= 50),
//   toArray(),
// );

// const largeNum$ = randomNum$.pipe(
//   filter(x => x > 50),
//   toArray(),
// );

// randomNum$.subscribe(x => log('random: ' + x));
// smallNum$.subscribe(x => log('small:', x));
// largeNum$.subscribe(x => log('large:', x));
