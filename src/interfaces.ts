export interface ITodo {
  title: string,
  id: number,
  completed: boolean,
}

export interface IUser {
  name: string,
  id: number,
  gender: string,
  height: string,
  mass: string,
  hair_color: string,
  skin_color: string
}