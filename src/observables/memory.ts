import { from, EMPTY, interval, generate, Observable, noop, Subject } from 'rxjs';
import { sequenceEqual, tap, take, scan, switchMap, map, delay } from 'rxjs/operators';
import { random } from '../helpers/math'
import { observerA, observerB } from '../helpers/observers';


interface ISetInfo {
  (message: string): void;
}

interface ISetActiveItem {
  (id: number): void;
}

interface IGameOver {
  (): void;
}

const COUNT_STEPS_DEFAULT = 2;

export default (size:number, setInfo:ISetInfo, setActiveItem: ISetActiveItem, gameOver: IGameOver) => {
    
  // const setInfo = (text: string) => document.getElementById('info')?.innerHTML = text;
  // const displayLevelChange = () => 
  // document.querySelectorAll('.child').forEach((c: HTMLElement) => c.style.background = 'gray');
  let input$: Subject<number>;

  const checkIfGameOver$ = (randomSequence: number[]) => (userSequence: number[]) => {
    console.log('checkIfGameOver', randomSequence, userSequence);
    return from(userSequence).pipe(
      sequenceEqual(from(randomSequence)),
      tap(match => {
        if(!match && userSequence.length === randomSequence.length) {
          gameOver();
          return noop;
        }
      })
    );
  }

  const takePlayerInput$ = (randomSequence: number[]) => (): Observable<any> => {
    console.log('randomSequence', randomSequence);
    input$ = new Subject();
    return input$.pipe(
      take(randomSequence.length),
      tap(i => console.log('takePlayerInput', i)),
      scan((acc: number[], id) => {
        if(id) {
          return [...acc, id as number]
        }
        return acc;
      }, []),
      switchMap(checkIfGameOver$(randomSequence)),
      switchMap(result => {
        if(result) {
          // displayLevelChange();
          return memoryGame$(randomSequence.length + 1);
        } else {
          return EMPTY;
        }
      })
    );
  }

  const showSequenceToMemorize$ = (memorySize: number) => (randomSequence: number[]) => interval(1000).pipe(
    tap(i => setInfo(i === memorySize - 1 ? `YOUR TURN` : `${memorySize - i} elements`)),
    take(randomSequence.length),
    map((index) => randomSequence[index]),
    tap(value => setActiveItem(value)),
    delay(500),
    tap(() => setActiveItem(0)),
    switchMap(takePlayerInput$(randomSequence))
  );

  const memoryGame$ = (steps: number) =>
    generate(1, x => x <= steps, x => x + 1)
      .pipe(
        scan((acc: number[], _: number): number[] => [...acc, random(size * size) + 1], []),
        switchMap(showSequenceToMemorize$(steps))
      );

  const game$ = memoryGame$(COUNT_STEPS_DEFAULT);

  const listener = game$.subscribe(observerA);
  
  return {
    stop: () => listener.unsubscribe(),
    nextSelect: (id: number) => {
      input$.next(id);
    }
  }
}
